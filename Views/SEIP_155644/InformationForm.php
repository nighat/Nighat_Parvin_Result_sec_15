<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Student Information and Marks Collection Form</title>

    <style>
        label{
            width: 80%;
            margin: 0 auto;
            display:block;
        }

        input{
            width: 80%;
            margin: 0 auto;
            display:block;
            color: seagreen;

        }
        .headline{
            text-align: center;
            border: solid 1px;

        }
        .studentInfo{
            border: solid 1px;
            margin-top: 50px;
        }

        .marksInfo{
            border: solid 1px;
            margin-top: 50px;
        }

        .submitForm{
            text-align: center;
            width:  40%;
            display: block;
            margin: 0 auto;
            border: solid 1px;
            color: seagreen;

        }

        .container{
            width: 75%;

            margin: 0 auto;
            display: block;
            height: 600px;
            border: 4px solid black;
            color: darkgreen;

        }

    </style>


</head>
<body>

<div class="container">



    <div class="headline">

        <h1> Student Information and Marks Collection Form</h1>

    </div>

    <form action="processFile.php" method="post">

        <div class="studentInfo">

            <label for="name"> Name:   </label> <br>
            <input type="text" name="name" > <br>

            <label for="studentID"> Student ID:   </label> <br>
            <input type="text" name="studentID" >  <br>

        </div>


        <div class="marksInfo">

            <label for="markBangla"> Mark Bangla:   </label> <br>
            <input type="number" step="any" name="markBangla" > <br>

            <label for="markEnglish"> Mark English:   </label> <br>
            <input type="number" step="any" name="markEnglish" >  <br>

            <label for="markMath"> Mark Math:   </label> <br>
            <input type="number" step="any" name="markMath" > <br>


        </div>


        <div class="submitForm">

            <input style="width: 100%" type="submit">

        </div>

    </form>

</div>

</body>
</html>