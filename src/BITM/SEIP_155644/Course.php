<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 1/27/2017
 * Time: 2:25 AM
 */

namespace App;


class Course
{
    private $markBangla;
    private $gradeBangla;

    private $markEnglish;
    private $gradeEnglish;
    private $markMath;
    private $gradeMath;

    public function setMarkBangla($markBangla)
    {
        $this->markBangla = $markBangla;
    }

    public function setGradeBangla()
    {
        $this->gradeBangla = $this->convertMark2grade($this->markBangla);
    }

    public function setMarkEnglish($markEnglish)
    {
        $this->markEnglish = $markEnglish;
    }

    public function setGradeEnglish()
    {
        $this->gradeEnglish = $this->convertMark2grade($this->markEnglish);
    }

    public function setMarkMath($markMath)
    {
        $this->markMath = $markMath;
    }

    public function setGradeMath()
    {
        $this->gradeMath =$this->convertMark2grade($this->markMath);
    }

    public function getMarkBangla()
    {
        return $this->markBangla;
    }

    public function getGradeBangla()
    {
        return $this->gradeBangla;
    }

    public function getMarkEnglish()
    {
        return $this->markEnglish;
    }

    public function getGradeEnglish()
    {
        return $this->gradeEnglish;
    }

    public function getMarkMath()
    {
        return $this->markMath;
    }

    public function getGradeMath()
    {
        return $this->gradeMath;
    }

    public function convertMark2grade($mark)
    {


        switch ($mark) {
            case ($mark > 89):
                $grade = "A+";
                Break;
            case ($mark > 79):
                $grade = "A";
                break;
            case ($mark > 67):
                $grade = "A-";
                break;
            case ($mark > 59):
                $grade = "B";
                break;
            case ($mark > 49):
                $grade = "C";
                break;
            case ($mark > 39):
                $grade = "D";
                break;
            default:
                $grade = "F";
        }
        return $grade;
    }
}