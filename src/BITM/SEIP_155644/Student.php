<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 1/27/2017
 * Time: 2:26 AM
 */

namespace App;


class Student
{
    private $name;
    private $studentID;


    public function setName($name)
    {
        $this->name = $name;
    }

    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStudentID()
    {
        return $this->studentID;
    }
}